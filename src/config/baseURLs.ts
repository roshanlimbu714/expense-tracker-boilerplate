export const PORT = 3030

export const BASE_URL_DEVELOP = process.env.REACT_APP_BASE_URL_DEVELOP;

export const BASE_URL_PROD_URL = process.env.REACT_APP_BASE_URL;
export const PROD_URL = process.env.REACT_APP_BASE_URL;
export const VITE_BASE_URL = process.env.REACT_APP_BASE_URL;
export const NODE_ENV = process.env.REACT_APP_NODE_ENV;
export const BASE_URL = process.env.REACT_APP_NODE_ENV === 'development' ?  BASE_URL_DEVELOP : PROD_URL;
export const IMAGE_URL = PROD_URL + 'image/get-image/'
