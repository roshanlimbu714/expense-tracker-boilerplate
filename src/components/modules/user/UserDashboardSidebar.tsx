import { Box, Tooltip } from '@mantine/core'
import { useState } from 'react';
import { LayoutDashboard, Users, ScreenShare, FileText } from 'tabler-icons-react';
import { NavLink } from 'react-router-dom';
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faDashboard } from "@fortawesome/free-solid-svg-icons";

export const UserDashboardSidebar = () => {
    //const iconDashboard = <FontAwesomeIcon icon={faDashboard} />
    const [active, setActive] = useState(0);

    const data = [
        { icon: <LayoutDashboard />, label: 'Dashboard', path: "/me" },
        { icon: <Users />, label: 'Users', path: "/me/users" },
        { icon: <ScreenShare />, label: 'Web Details', path: "/me/webdetails" },
        { icon: <FileText />, label: 'Complaints', path: "/me/complaints" },
        { icon: <FileText />, label: 'Queries', path: "/me/queries" },
    ]

    const items = data.map((item, index) => (
        <NavLink className='mt-xs font-semibold w-full' to={item.path} key={item.label} end={item.label === 'Dashboard'}>
            <Tooltip label={item.label} position={'right'}>
                <div className={'flex items-center hover:bg-gray-200 justitfy-center h-[60px] w-[60px] rounded-md py-xs px-sm'}>
                    {item.icon}
                </div>
            </Tooltip>
        </NavLink>
    ));

    return <Box mt={'sm'} className={'w-full'}>{items}</Box>
}