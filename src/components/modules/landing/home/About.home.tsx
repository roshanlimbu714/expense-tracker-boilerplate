import { Grid } from '@mantine/core'

export const AboutHome = () => {
    return <section className={'px-wrapper py-xl'}>
        <Grid>
            <Grid.Col span={4}>
                <div className="text-4xl font-bold">Keep spending </div>
                <div className="text-4xl font-bold">structured</div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, commodi tempore. Deserunt hic inventore quam? Eos, iusto, optio! Corporis eaque error in ipsum iure laboriosam minus nobis numquam quas qui!
                </p>
            </Grid.Col>
            <Grid.Col span={8}>
                <Grid>
                    <Grid.Col span={6}>
                        <div className="text-xl font-bold">Feature 1</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, est, velit! Dolores ex itaque pariatur vero voluptas. Accusamus at earum harum pariatur ut! Deleniti maxime mollitia, nulla odit omnis ut!</p>
                    </Grid.Col>
                    <Grid.Col span={6}>
                        <div className="text-xl font-bold">Feature 1</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, est, velit! Dolores ex itaque pariatur vero voluptas. Accusamus at earum harum pariatur ut! Deleniti maxime mollitia, nulla odit omnis ut!</p>
                    </Grid.Col>
                    <Grid.Col span={6}>
                        <div className="text-xl font-bold">Feature 1</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, est, velit! Dolores ex itaque pariatur vero voluptas. Accusamus at earum harum pariatur ut! Deleniti maxime mollitia, nulla odit omnis ut!</p>
                    </Grid.Col>
                    <Grid.Col span={6}>
                        <div className="text-lg">Feature 1</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, est, velit! Dolores ex itaque pariatur vero voluptas. Accusamus at earum harum pariatur ut! Deleniti maxime mollitia, nulla odit omnis ut!</p>
                    </Grid.Col>
                </Grid>
            </Grid.Col>
        </Grid>
    </section>;
}