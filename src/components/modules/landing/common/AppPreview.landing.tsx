import { Grid } from '@mantine/core'
import dashboardPreview from '../../../../assets/images/dashboardPreview.png'
import { CircleCheck } from 'tabler-icons-react'

export const AppPreviewLanding = () => {
    return <section className={'pl-wrapper'}>
        <Grid>
            <Grid.Col span={5} className={'flex flex-col justify-center'}>
                <div className="text-4xl font-bold ">
                    Join 1000+ users in smarter spending
                </div>
                <div className="join-features mt-md">
                    <div className="join-feature flex items-center">
                        <CircleCheck />
                        <div className={'pl-sm text-lg mb-sm font-bold'}>joining reason</div>
                    </div>
                    <div className="join-feature flex items-center">
                        <CircleCheck />
                        <div className={'pl-sm text-lg mb-sm font-bold'}>joining reason</div>
                    </div>
                    <div className="join-feature flex items-center">
                        <CircleCheck />
                        <div className={'pl-sm text-lg mb-sm font-bold'}>joining reason</div>
                    </div>
                </div>
            </Grid.Col>
            <Grid.Col span={7} m={0} p={0}>
                <img src={dashboardPreview} className={'h-[400px] w-full object-cover'}
                     style={{ objectPosition: 'left top' }} alt="" />
            </Grid.Col>
        </Grid>
    </section>
}