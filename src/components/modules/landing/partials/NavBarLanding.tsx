 import { Button } from "@mantine/core"
import logo from "../../../../assets/Logo.svg"
 import { NavLink, useNavigate } from 'react-router-dom'
export const NavBarLanding = () => {
    const navigate = useNavigate();



    return <nav className="flex items-center justify-between px-wrapper h-[80px] fixed w-full z-40">
        <div className="Logo"><img src={logo} alt="Expense Tracker Logo" /></div>
        <div className="nav-items flex items-center">
            <NavLink to={'/'} className="nav-item px-md">Home</NavLink>
            <NavLink to={'/features'} className="nav-item px-md">Features</NavLink>
            <NavLink to={'/contact'} className="nav-item px-md">Contact</NavLink>
            <div className="nav-item">
                <div className="nav-buttons flex">
                    <Button onClick={()=>navigate('/auth/')} variant="filled" color="rgba(0, 0, 0, 1)">Sign In</Button>
                    <Button onClick={()=>navigate('/auth/signup')} className="ml-xs" variant="outline" color="rgba(0, 0, 0, 1)">Sign Up</Button>
                </div>
            </div>

        </div>
    </nav>
}