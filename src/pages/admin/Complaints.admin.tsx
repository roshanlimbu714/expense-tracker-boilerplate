import { ActionIcon, Tabs } from '@mantine/core'
import { TableLayout } from "../../components/modules/dashboard/common/Table"
import { useEffect, useState } from 'react'
import { TabsLayout } from "../../components/modules/dashboard/common/Tabs";
import { ExpTable } from "../../components/common/ExpTable";
import { Eye, Pencil } from 'tabler-icons-react'
import { useNavigate } from 'react-router-dom'
import { GetComplaints, GetComplaintsByStatus } from '../../API/Complaints/GetComplaints'

export const Complaints=()=>{
    const [activeTab, setActiveTab] = useState<string | null>('unacknowledged');
    const [data, setData] = useState([]);
    const navigate= useNavigate();

    const tabs = [
        { label: "Unacknowledged", value: "unacknowledged" },
        { label: "Acknowledged", value: "acknowledged" },
    ]

    const cols = [
        "complaint",
        "profile",
        "joineddate",
        "status"
    ]

    const [tabledata,setTableData] = useState([] as any);
    const ActionsCol = (props:any)=>{
        return (
            <div className="flex justify-center gap-xs">
                <ActionIcon
                    variant="light"
                    onClick={() => navigate(`/users/${props.id}`)}
                >
                    <Eye size={20} />
                </ActionIcon>
                <ActionIcon
                    variant="light"
                    onClick={() => navigate(`/users/${props.id}/edit`)}
                >
                    <Pencil size={20} />
                </ActionIcon>
            </div>
        )
    }

    const headers = [
        { label: 'Complaint', name:'complaint', width: '35%'},
        { label: 'Profile', name:'fullname', width: '25%',},
        { label: 'Created At', name:'createdAt', width: '15%'},
        { label: 'Status', name:'status', type:'badge', width: '15%', align: 'center'},
        { label: 'Actions', name:'actions',width:'15%', align: 'center',type: 'action', actionCol: ActionsCol},
    ]

    const loadData =async () => {
        const res = await GetComplaintsByStatus(activeTab?.toUpperCase() ?? 'UNACKNOWLEDGED');
    }

    useEffect(() => {
        loadData()
    }, [activeTab])

    return <div>
    <div className="font-bold text-lg">Users</div>
    <div>
        <Tabs value={activeTab} onChange={setActiveTab} color="orange">
            <TabsLayout tabsList={tabs}/>

            <Tabs.Panel value="unacknowledged"  className="py-md">
                <ExpTable data={tabledata} cols={cols} headers={headers} />
            </Tabs.Panel>

            <Tabs.Panel value="acknowledged"  className="py-md">
                <ExpTable data={tabledata} cols={cols} headers={headers} />
            </Tabs.Panel>
        </Tabs>
    </div>
</div>

}